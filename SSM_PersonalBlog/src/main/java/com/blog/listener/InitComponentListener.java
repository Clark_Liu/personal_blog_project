package com.blog.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.blog.listener.event.InitComponentEvent;
import com.blog.service.common.ICommonService;

@Component
public class InitComponentListener implements ApplicationListener<InitComponentEvent>{

	@Autowired
	private ICommonService commonService;
	
	public void onApplicationEvent(InitComponentEvent event) {
		commonService.commonRefreshApplicationBeanInfo(event.getRequest());
		
	}




}
