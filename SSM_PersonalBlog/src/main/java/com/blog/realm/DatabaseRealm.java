package com.blog.realm;



import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blog.entity.Blogger;
import com.blog.service.background.LoginSerivce;
import com.blog.service.background.Impl.ILoginService;


public class DatabaseRealm extends AuthorizingRealm {
	
	@Autowired
	private ILoginService loginSerivce;
	/**
	 * 角色与权限
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
		/*//能进入到这里，表示账号已经通过验证了
		String userName =(String) principalCollection.getPrimaryPrincipal();
		//通过service获取角色和权限
		Set<String> permissions = permissionService.listPermissions(userName);
		Set<String> roles = roleService.listRoles(userName);
		
		//授权对象
		SimpleAuthorizationInfo s = new SimpleAuthorizationInfo();
		//把通过service获取到的角色和权限放进去
		s.setStringPermissions(permissions);
		s.setRoles(roles);
		return s;*/
		return null;
	}
	/**
	 * 登录与验证
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		//获取账号密码
		UsernamePasswordToken t = (UsernamePasswordToken) token;
		String userName= token.getPrincipal().toString();
		String password= new String( t.getPassword());
		//获取数据库中的密码
		Blogger blogger = loginSerivce.getBloggerByName(userName);

		//如果为空就是账号不存在，如果不相同就是密码错误，但是都抛出AuthenticationException，而不是抛出具体错误原因，免得给破解者提供帮助信息
		if(null==blogger.getPassword() || !blogger.getPassword().equals(password)) 
			throw new AuthenticationException();
		
		//认证信息里存放账号密码, getName() 是当前Realm的继承方法,通常返回当前类名 :databaseRealm
		SimpleAuthenticationInfo a = new SimpleAuthenticationInfo(blogger,blogger.getPassword(),getName());
		return a;
	}

}
