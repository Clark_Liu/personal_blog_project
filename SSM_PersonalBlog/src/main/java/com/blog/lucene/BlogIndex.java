package com.blog.lucene;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Fragmenter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.blog.entity.Blog;
import com.blog.util.DateUtil;
import com.blog.util.WebFileUtil;


public class BlogIndex {
	

	private Directory dir=null;
	
	//private final String indexStorePath="C://lucene";
	//lucene索引文件 存在rootPath+indexFile中
	private final String indexFile="static/luceneIndex";
	
	private static IKAnalyzer analyzer = new IKAnalyzer();
	
	public BlogIndex() {
		super();
	}
	
	/**
	 * 获取IndexWriter实例
	 * @return
	 * @throws Exception
	 */
	private IndexWriter getWriter(HttpServletRequest request) throws Exception{
		//String str=WebFileUtil.getSystemRootPath(request)+indexFile;
		dir=FSDirectory.open(Paths.get(WebFileUtil.getSystemRootPath(request)+indexFile));
//		Path path = new File("F:\\git\\personal_blog_project\\SSM_PersonalBlog\\src\\main\\webapp\\static\\luceneIndex").toPath();
//		dir=FSDirectory.open(path);
		//中文分词器
		IndexWriterConfig iwc=new IndexWriterConfig(analyzer);
		IndexWriter writer=new IndexWriter(dir, iwc);
		return writer;
	}
	
	/**
	 * 初始化生成所有已存在的博客索引文件
	 * @param blogList 所有博客实例的集合
	 * @param request
	 * @throws Exception
	 */
	public void createIndexInit(List<Blog> blogList,HttpServletRequest request) throws Exception {
		//获取 索引写入器
		IndexWriter writer=getWriter(request);
		writer.deleteAll();
		for (Blog b : blogList) {
			addOrUpdateDoc(writer, b,1);
		}
		writer.close();
	}
	
	/**
	 * 生成或修改索引文件
	 * @param writer
	 * @param b
	 * @throws IOException
	 */
	public void addOrUpdateDoc(IndexWriter writer,Blog b,int temp) throws IOException {
		Document doc = new Document();
		doc.add(new StringField("id",String.valueOf(b.getId()),Field.Store.YES));
		doc.add(new TextField("title",b.getTitle(),Field.Store.YES));
		doc.add(new StringField("releaseDate",DateUtil.formatDate(b.getReleaseDate(), "yyyy-MM-dd"),Field.Store.YES));
		doc.add(new TextField("content",String.valueOf(b.getContentNoTag()),Field.Store.YES));
		if(temp == 1) {
			writer.addDocument(doc);
		}else if(temp == 0) {
			writer.updateDocument(new Term("id", String.valueOf(b.getId())), doc);
		}
	}
	
	/**
	 * 新增单独一个的博客索引文件
	 * @param blog
	 * @throws Exception
	 */
	public void addIndex(Blog blog,HttpServletRequest request)throws Exception{
		IndexWriter writer=getWriter(request);
		addOrUpdateDoc(writer, blog,1);
		writer.close();
	}
	/**
	 * 修改博客索引文件
	 * @param blog
	 * @throws Exception
	 */
	public void updateIndex(Blog blog,HttpServletRequest request)throws Exception{
		IndexWriter writer=getWriter(request);
		addOrUpdateDoc(writer,blog,0);
		writer.close();
	}
	
	/**
	 * 删除指定的博客索引文件
	 * @param blogId
	 * @throws Exception
	 */
	public void deleteIndex(Blog blog,HttpServletRequest request)throws Exception{
		IndexWriter writer=getWriter(request);
		writer.deleteDocuments(new Term("id",blog.getId()+""));
		writer.forceMergeDeletes(); // 强制删除
		writer.commit();
		writer.close();
	}
	
	
	/**
	 * 根据关键词进行全文检索
	 * @param queryWord
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public List<Blog> searchBlog(String queryWord,HttpServletRequest request) throws Exception{
		//1.找到索引文件存放的位置
		dir=FSDirectory.open(Paths.get(WebFileUtil.getSystemRootPath(request)+indexFile));
		//2.创建reader 来读取索引文件
		IndexReader reader = DirectoryReader.open(dir);
		//3.创建searcher搜索器
		IndexSearcher searcher = new IndexSearcher(reader);
		//多条件搜索要用到的
		BooleanQuery.Builder booleanQuery = new BooleanQuery.Builder();
		
		//4.创建parser解析器 参数为 目标字段 和 分词器
		QueryParser parser = new QueryParser("title",analyzer);//条件一
		QueryParser parser2 = new QueryParser("content",analyzer);//条件二
		//5.用 搜索关键词 利用parser解析器出结果
		Query query = parser.parse(queryWord);//条件一
		Query query2 = parser2.parse(queryWord);//条件二
		booleanQuery.add(query,BooleanClause.Occur.SHOULD);
		booleanQuery.add(query2,BooleanClause.Occur.SHOULD);
		
		//设置高亮显示配置器
		QueryScorer scorer=new QueryScorer(query);
		Fragmenter fragmenter = new SimpleSpanFragmenter(scorer); 
		SimpleHTMLFormatter simpleHTMLFormatter=new SimpleHTMLFormatter("<b><font color='red'>","</font></b>");
		Highlighter highlighter=new Highlighter(simpleHTMLFormatter, scorer);
		highlighter.setTextFragmenter(fragmenter); 
		
		//获取到符合条件的记录,并且截取前100条记录,生成的是一个数组
		TopDocs tds = searcher.search(booleanQuery.build(),100);
		ScoreDoc[] scoreDocs = tds.scoreDocs;
		//新建一个实体类的集合
		List<Blog> blogList=new LinkedList<Blog>();
		//遍历doc数组,将符合条件的记录一一放进集合中去
		for(ScoreDoc scoreDoc : scoreDocs) {
			Document doc = searcher.doc(scoreDoc.doc);
			Blog blog=new Blog();
			blog.setId(Integer.parseInt(doc.get("id")));
			blog.setReleaseDateStr(doc.get(("releaseDate")));
			String title=doc.get("title");
			String content=doc.get("content");
			
			//对目标词汇进行高亮显示
			TokenStream tokenStream = analyzer.tokenStream("title", new StringReader(title));
			String hTitle=highlighter.getBestFragment(tokenStream, title);
			
			if(StringUtils.isEmpty(hTitle)){
				blog.setTitle(title);
			}else{
				blog.setTitle(hTitle);					
			}
			
			tokenStream = analyzer.tokenStream("content", new StringReader(content)); 
			String hContent=highlighter.getBestFragment(tokenStream, content);
			if(StringUtils.isEmpty(hContent)){
				if(content.length()<=200){
					blog.setContent(content);
				}else{
					blog.setContent(content.substring(0, 200));						
				}
			}else{
				blog.setContent(hContent);					
			}
			blogList.add(blog);
		}
		return blogList;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
