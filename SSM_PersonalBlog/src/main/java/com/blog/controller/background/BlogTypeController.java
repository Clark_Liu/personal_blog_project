package com.blog.controller.background;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.blog.entity.BlogType;
import com.blog.service.background.IBlogService;
import com.blog.service.background.IBlogTypeService;
import com.blog.listener.event.InitComponentEvent;

@Controller
@RequestMapping("/blogType")
public class BlogTypeController {
	@Autowired
	private IBlogTypeService blogTypeService;
	@Autowired
	private IBlogService blogService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@RequestMapping("/getBlogType")
	@ResponseBody
	public String getBlogType() {
		List<BlogType> typeList = blogTypeService.getBlogType();
		JSONObject js = new JSONObject();
		js.put("typeList", typeList);
		return js.toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping("/getAllBlogType")
	@ResponseBody
	public String getAllBlogType(Integer page,Integer rows) {
		HashMap<String,Object> map = blogTypeService.getAllBlogType(page,rows);
		List<BlogType> blogTypeList = (List<BlogType>) map.get("blogTypeList");
		JSONArray jsArr = new JSONArray();
		for(int i = 0;i<blogTypeList.size();i++) {
			JSONObject js = new JSONObject();
			js.put("id", blogTypeList.get(i).getId());
			js.put("typeName", blogTypeList.get(i).getTypeName());
			js.put("orderNo", blogTypeList.get(i).getOrderNo());
			js.put("blogAmount", blogService.getBlogAmountByBlogType(blogTypeList.get(i).getId()));
			jsArr.add(js);
		}
		JSONObject js = new JSONObject();
		js.put("total", map.get("total"));
		js.put("rows", jsArr);
		return js.toString();
	}
	
	@RequestMapping("/addNewBlogType")
	@ResponseBody
	public String addNewBlogType(BlogType blogType,HttpServletRequest request) {
		int i = blogTypeService.addNewBlogType(blogType);
		if(i>0) {
			this.commonRefreshSystemCache(request);
			return "success";
		}
		return "false";
	}
	
	
	@RequestMapping("/getBlogTypeById")
	@ResponseBody
	public String getBlogTypeById(Integer id) {
		BlogType blogType = blogTypeService.getBlogTypeById(id);
		JSONObject js = new JSONObject();
		js.put("blogType", blogType);
		return js.toString();
	}
	
	@RequestMapping("/updateBlogTypeById")
	@ResponseBody
	public String updateBlogTypeById(BlogType blogType,HttpServletRequest request) {
		int i = blogTypeService.updateBlogTypeById(blogType);
		if(i>0) {
			this.commonRefreshSystemCache(request);
			return "success";
		}
		return "false";
	}
	
	@RequestMapping("/removeBlogTypeById")
	@ResponseBody
	public String removeBlogTypeById(Long[] arrId,HttpServletRequest request) {
		blogTypeService.removeBlogTyepByArrId(arrId);
		this.commonRefreshSystemCache(request);
		return "success";
	}
	
	
	/**
	 * 异步刷新博客相关信息
	 * @param request
	 */
	private void commonRefreshSystemCache(HttpServletRequest request){
		try {
			InitComponentEvent event=new InitComponentEvent(this, 0,request);
			publisher.publishEvent(event);
		} catch (Exception e) {}
	}
	
}
