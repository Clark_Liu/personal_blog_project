package com.blog.controller.background;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.blog.service.common.ICommonService;

@Controller
public class SystemController {
	@Autowired
	private ICommonService commonService;

	@RequestMapping("/refreshSystem")
	@ResponseBody
	public String refreshSystem(HttpServletResponse response,HttpServletRequest request)throws Exception{
		commonService.commonRefreshApplicationBeanInfo(request);
		return "success";
	}
}
