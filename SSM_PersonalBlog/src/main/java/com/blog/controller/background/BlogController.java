package com.blog.controller.background;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.blog.entity.Blog;
import com.blog.listener.event.InitComponentEvent;
import com.blog.service.background.IBlogService;
import com.blog.service.background.IBlogTypeService;
import com.blog.util.DateUtil;
import com.blog.util.WebFileOperationUtil;

@Controller
@RequestMapping("/blog")
@SuppressWarnings("all")
public class BlogController {
	@Autowired
	private IBlogService blogService;
	@Autowired
	private IBlogTypeService blogTypeService;
	@Autowired
	private ApplicationEventPublisher publisher;
	
	/**
	 * 保存新博客
	 * @param blog 博客实体类
	 * @return
	 */
	@RequestMapping("/saveBlog")
	@ResponseBody
	public String saveBlog(Blog blog,HttpServletResponse response,HttpServletRequest request) {
		
		String newContent=WebFileOperationUtil.copyImageInUeditor(request, blog.getContent());
		
		blog.setContent(newContent);
		
		
		int i = blogService.saveBlog(blog);
		if(i!=0) {
			this.commonRefreshSystemCache(request);
			return "success";
		}else {
			return "false";
		}
	}
	
	@SuppressWarnings("all")
	@RequestMapping("/getAllBlog")
	@ResponseBody
	public String getAllBlog(Integer page,Integer rows) {
		HashMap<String,Object> map = blogService.getAllBlog(page,rows);
		List<Blog> blogList = (List<Blog>) map.get("blogList");
		JSONArray jsArr = new JSONArray();
		for(int i = 0;i<blogList.size();i++) {
			JSONObject js = new JSONObject();
			js.put("id", blogList.get(i).getId());
			js.put("title", blogList.get(i).getTitle());
			js.put("releaseDate", DateUtil.formatDate(blogList.get(i).getReleaseDate(), "yyyy-MM-dd"));
			js.put("typeName", blogTypeService.getBlogTypeById(blogList.get(i).getTypeId()).getTypeName());
			jsArr.add(js);
		}
		JSONObject js = new JSONObject();
		js.put("total", map.get("total"));
		js.put("rows", jsArr);
		return js.toString();
	}
	
	@RequestMapping("/getBlogById")
	@ResponseBody
	public String getBlogById(Integer id) {
		Blog b = blogService.getBlogById(id);
		JSONObject js = new JSONObject();
		js.put("b", b);
		return js.toString();
	}
	
	
	@RequestMapping("/editSaveBlog")
	@ResponseBody
	public String editSaveBlog(HttpServletRequest request,Blog b) {
		String newContent=WebFileOperationUtil.copyImageInUeditor(request, b.getContent());
		
		b.setContent(newContent);
		int i =blogService.editSaveBlog(b);
		if (i !=0) {
			this.commonRefreshSystemCache(request);
			return "success";
		}
		return "false";
	}
	
	
	@RequestMapping("/removeBlog")
	@ResponseBody
	public String removeBlog(Long[] arrId,HttpServletRequest request) {
		int i = blogService.removeBlog(arrId);
		if(i == arrId.length) {
			this.commonRefreshSystemCache(request);
			return "success";
		}else {
			blogService.recoverBlog(arrId);
			return "false";
		}
	}
	
	@RequestMapping("/searchBlogByTitle")
	@ResponseBody
	public String searchBlogByTitle(Integer page,Integer rows,String title) {
		HashMap<String,Object> map = blogService.searchBlogByTitle(page, rows, title);
		
		List<Blog> blogList = (List<Blog>) map.get("blogList");
		JSONArray jsArr = new JSONArray();
		for(int i = 0;i<blogList.size();i++) {
			JSONObject js = new JSONObject();
			js.put("id", blogList.get(i).getId());
			js.put("title", blogList.get(i).getTitle());
			js.put("releaseDate", DateUtil.formatDate(blogList.get(i).getReleaseDate(), "yyyy-MM-dd"));
			js.put("typeName", blogTypeService.getBlogTypeById(blogList.get(i).getTypeId()).getTypeName());
			jsArr.add(js);
		}
		JSONObject js = new JSONObject();
		js.put("total", map.get("total"));
		js.put("rows", jsArr);
		return js.toString();
	}
	
	/**
	 * 异步刷新博客类别以及博客的数量
	 * @param request
	 */
	private void commonRefreshSystemCache(HttpServletRequest request){
		try {
			InitComponentEvent event=new InitComponentEvent(this, 0,request);
			publisher.publishEvent(event);
		} catch (Exception e) {}
	}
	
}
