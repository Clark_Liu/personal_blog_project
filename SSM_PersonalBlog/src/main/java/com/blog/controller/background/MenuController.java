package com.blog.controller.background;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.blog.entity.Menu;
import com.blog.service.background.IMenuService;

@Controller
@RequestMapping("/menu")
public class MenuController {
	@Autowired
	private IMenuService menuService;
	
	/**
	 * 获取所有父级菜单
	 * @return
	 */
	@RequestMapping("/getParentMenu")
	@ResponseBody
	public String getParentMenu() {
		List<Menu> menuList = menuService.getParentMenu();
		JSONObject js = new JSONObject();
		js.put("menuList", menuList);
		return js.toString();
	}
	/**
	 * 获取相应的子级菜单
	 * @param parentName 父级菜单名称
	 * @return json字符串数组
	 */
	@RequestMapping("/getChildMenu")
	@ResponseBody
	public String getChildMenu(String parentName) {
		List<Menu> menuList = menuService.getChildMenu(parentName);
		JSONArray jsArr = new JSONArray();
		for(Menu me : menuList) {
			JSONObject js = new JSONObject();
			js.put("id", me.getId());
			js.put("text", me.getMenuName());
			js.put("iconCls", me.getMenuIcon());
			js.put("url", me.getMenuLink());
			jsArr.add(js);
		}
		return jsArr.toString();
	}
}
