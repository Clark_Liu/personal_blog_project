package com.blog.controller.background;


import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.blog.entity.Blogger;
import com.blog.listener.event.InitComponentEvent;
import com.blog.service.background.IBloggerService;
import com.blog.util.DateUtil;
import com.blog.util.WebFileUtil;

@Controller
@RequestMapping("/blogger")
public class BloggerController {
	
	@Autowired
	private IBloggerService bloggerService;
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@RequestMapping("/getBlogger")
	@ResponseBody
	public String getBlogger() {
		Subject currentUser = SecurityUtils.getSubject();
		Blogger currentUserIntence = (Blogger)currentUser.getPrincipal();
		Blogger user = bloggerService.getBloggerByUserName(currentUserIntence.getUserName());
		JSONObject js = new JSONObject();
		js.put("user", user);
		return js.toString();
	}
	
	
	@RequestMapping("/saveBloggerInfo")
	@ResponseBody
	public String saveBloggerInfo(Blogger blogger,@RequestParam("file")MultipartFile file,HttpServletRequest request) throws Exception {
		Subject currentUser = SecurityUtils.getSubject();
		Blogger currentBlogger = (Blogger)currentUser.getPrincipal();
		if(!currentBlogger.getUserName().equals(blogger.getUserName())) {
			Blogger bloggerFromDB = bloggerService.getBloggerByUserName(blogger.getUserName());
			if(bloggerFromDB!=null) {
				return "false";
			}
		}
		String fileName = file.getOriginalFilename();
		if(fileName!=""){
			String fileType = fileName.split("\\.")[1];
			String filePath=WebFileUtil.getSystemRootPath(request);
			String newFileName = DateUtil.getCurrentDateStr()+blogger.getUserName()+"."+fileType;
			InputStream is = null;
			InputStream is2 = null;
			OutputStream os = null;
			OutputStream os2 = null;
			try {
				//服务器项目路径
				os = new FileOutputStream(filePath+"static\\userFileUpload\\headImg\\"+newFileName);
				//本地路径
				os2 = new FileOutputStream("F:\\git\\personal_blog_project\\SSM_PersonalBlog\\src\\main\\webapp\\static\\userFileUpload\\headImg\\"+newFileName);
				is = file.getInputStream();
				is2 = file.getInputStream();
				IOUtils.copy(is, os);
				IOUtils.copy(is2, os2);
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				is.close();
				os.close();
				os2.close();
				is2.close();
			}
			blogger.setImageName(newFileName);
		}
		
		int i = bloggerService.updateBlogger(blogger);
		Blogger user = (Blogger) SecurityUtils.getSubject().getPrincipal();
		user.setUserName(blogger.getUserName());
		
		if(i>0) {
			this.commonRefreshSystemCache(request);
			return "success";
		}
		return "false";
	}
	
	@RequestMapping("/updatePassword")
	@ResponseBody
	public String updatePassword(String userName,String password,int id) {
		Subject currentUser = SecurityUtils.getSubject();
		Blogger currentBlogger = (Blogger)currentUser.getPrincipal();
		Blogger newBloggerInfo = new Blogger();
		newBloggerInfo.setId(id);
		if(!currentBlogger.getUserName().equals(userName)) {
			Blogger bloggerFromDB = bloggerService.getBloggerByUserName(userName);
			if(bloggerFromDB==null) {
				newBloggerInfo.setUserName(userName);
			}else {
				return "false";
			}
		}
		newBloggerInfo.setPassword(password);
		int i = bloggerService.updateBlogger(newBloggerInfo);
		if(i>0) {
			return "success";
		}
		return "false";
	}
	
	
	/**
	 * 异步刷新博客类别以及博客的数量
	 * @param request
	 */
	private void commonRefreshSystemCache(HttpServletRequest request){
		try {
			InitComponentEvent event=new InitComponentEvent(this, 0,request);
			publisher.publishEvent(event);
		} catch (Exception e) {}
	}
}
