package com.blog.controller.background;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.blog.entity.Blog;
import com.blog.entity.Comment;
import com.blog.service.background.IBlogService;
import com.blog.service.background.ICommentService;
import com.blog.util.DateUtil;

@Controller
@RequestMapping("/comment")
public class CommentController {
	@Autowired
	private ICommentService commentService;
	@Autowired
	private IBlogService blogService;
	
	@RequestMapping("/saveComments")
	@ResponseBody
	public String saveComments(Comment comment,String imageCode,HttpServletRequest request,
			HttpServletResponse response,HttpSession session) {
		String sRand=(String) session.getAttribute("sRand");
		
		//操作的记录条数
		int resultTotal=0; 
		
		if(!imageCode.equals(sRand)) {
			return "falseCode";
		}else {
			//获取用户IP
			String userIp=request.getRemoteAddr(); 
			comment.setUserIp(userIp);
			comment.setCommentDate(new Date());
			resultTotal = commentService.save(comment);
			Blog blog = blogService.getBlogById(comment.getBlogId());
			blog.setReplyHit(blog.getReplyHit()+1);
			blogService.editSaveBlog(blog);
			
			if(resultTotal > 0) {
				return "success";
			}else {
				return "false";
			}
		}
	}
	
	@RequestMapping("/reviewComments")
	@ResponseBody
	public String reviewComments(Integer page,Integer rows) {
		HashMap<String,Object> map = commentService.getReviewComments(page,rows);
		List<Comment> commentList = (List<Comment>) map.get("commentList");
		JSONArray jsArr = new JSONArray();
		for(int i = 0;i<commentList.size();i++) {
			JSONObject js = new JSONObject();
			js.put("id", commentList.get(i).getId());
			js.put("blogTitle", blogService.getBlogById(commentList.get(i).getBlogId()).getTitle());
			js.put("userIp", commentList.get(i).getUserIp());
			js.put("content", commentList.get(i).getContent());
			js.put("commentDate", DateUtil.formatDate(commentList.get(i).getCommentDate(), "yyyy-MM-dd"));
			jsArr.add(js);
		}
		JSONObject js = new JSONObject();
		js.put("total", map.get("total"));
		js.put("rows", jsArr);
		return js.toString();
	}
	
	
	
	@RequestMapping("/updateReviewState")
	@ResponseBody
	public String updateReviewState(Integer temp,int[] arrId) {
		int i = commentService.updateReviewState(temp,arrId);
		if(i==arrId.length) {
			return "success";
		}else {
			return "false";
		}
	}
	
	
	@RequestMapping("/getAllComments")
	@ResponseBody
	public String getAllComments(Integer page,Integer rows) {
		HashMap<String,Object> map = commentService.getAllComments(page,rows);
		List<Comment> commentList = (List<Comment>) map.get("commentList");
		JSONArray jsArr = new JSONArray();
		for(int i = 0;i<commentList.size();i++) {
			JSONObject js = new JSONObject();
			js.put("id", commentList.get(i).getId());
			js.put("blogTitle", blogService.getBlogById(commentList.get(i).getBlogId()).getTitle());
			js.put("userIp", commentList.get(i).getUserIp());
			js.put("content", commentList.get(i).getContent());
			js.put("commentDate", DateUtil.formatDate(commentList.get(i).getCommentDate(), "yyyy-MM-dd"));
			String state;
			if(commentList.get(i).getState() == -1) {
				state="审核未通过";
			}else if(commentList.get(i).getState() == 0) {
				state="待审核";
			}else {
				state="审核通过";
			}
			js.put("state", state);
			jsArr.add(js);
		}
		JSONObject js = new JSONObject();
		js.put("total", map.get("total"));
		js.put("rows", jsArr);
		return js.toString();
	}
	
	
	@RequestMapping("/removeComments")
	@ResponseBody
	public String removeComments(int[] arrId) {
		commentService.removeComments(arrId);
		return "success";
	}
	
}	
