package com.blog.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.blog.entity.Blogger;

@Controller
@RequestMapping("/background")
public class SkipPageController {

	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String login(Model model) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated()) {
			return "redirect:/background/backgroundIndex";
		}
		return "/background/login.jsp";
	}
	
	@RequestMapping("/backgroundIndex")
	public String background(Model model) {
		Subject currentUser = SecurityUtils.getSubject();
		Blogger blogger = (Blogger)currentUser.getPrincipal();
		model.addAttribute("userName", blogger.getUserName());
		model.addAttribute("userId", blogger.getId());
		return "/background/index.jsp";
	}
	
	@RequestMapping("/addNewBlog")
	public String addNewBlog() {
		return "/background/addNewBlog.jsp";
	}
	
	@RequestMapping("/blogInfoManagement")
	public String blogInfoManagement() {
		return "/background/blogInfoManagement.jsp";
	}
	
	@RequestMapping("/blogTypeManagement")
	public String blogTypeManagement() {
		return "/background/blogTypeManagement.jsp";
	}
	
	@RequestMapping("/commentReview")
	public String commentReview() {
		return "/background/commentReview.jsp";
	}
	
	@RequestMapping("/commentManagement")
	public String commentManagement() {
		return "/background/commentManagement.jsp";
	}
	
	@RequestMapping("/personalInfoEdit")
	public String personalInfoEdit(){
		return "/background/personalInfoEdit.jsp";
	}
	@RequestMapping("/linkManagement")
	public String linkManagement() {
		return "/background/linkManagement.jsp";
	}
	
	@RequestMapping("/editBlog")
	public String editBlog(int id) {
		return "/background/editBlog.jsp?id="+id;
	}
	
	@RequestMapping("/forgroundIndex")
	public String forgroundIndex() {
		return "/forground/index.jsp";
	}
	
	
	
	
}
