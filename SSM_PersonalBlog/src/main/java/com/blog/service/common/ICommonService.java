package com.blog.service.common;

import javax.servlet.http.HttpServletRequest;

public interface ICommonService {

	void commonRefreshApplicationBeanInfo(HttpServletRequest request);

}
