package com.blog.service.common.Impl;


import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.blog.entity.Blogger;
import com.blog.entity.Link;
import com.blog.service.background.IBlogService;
import com.blog.service.background.IBlogTypeService;
import com.blog.service.background.IBloggerService;
import com.blog.service.background.ILinkService;
import com.blog.service.common.ICommonService;
import com.blog.entity.Blog;
import com.blog.entity.BlogType;

@Service
public class CommonService implements ICommonService {
	@Autowired
	private IBloggerService bloggerService;
	@Autowired
	private IBlogTypeService blogTypeService;
	@Autowired
	private IBlogService blogService;
	@Autowired
	private ILinkService linkService;
	
	public void commonRefreshApplicationBeanInfo(HttpServletRequest request) {
		//获取上下文应用程序:javaweb页面作用域-application
		ServletContext application = RequestContextUtils.getWebApplicationContext(request).getServletContext();
		
		//查询博主信息
		Blogger blogger = bloggerService.getBlogger();
		//防止把密码传到前台去
		blogger.setPassword(null);
		application.setAttribute("blogger", blogger);
		//查询博客类别以及博客的数量
		List<BlogType> blogTypeCountList=blogTypeService.countList(); 
		application.setAttribute("blogTypeCountList", blogTypeCountList);		
		//根据日期分组查询博客
		List<Blog> blogCountList=blogService.countList(); 
		application.setAttribute("blogCountList", blogCountList);
		//获取所有友情链接
		List<Link> linkList = linkService.getAllLink();
		application.setAttribute("linkList", linkList);
		
	}


}
