package com.blog.service.common.Impl;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.blog.entity.Blog;
import com.blog.entity.BlogType;
import com.blog.entity.Blogger;
import com.blog.entity.Link;
import com.blog.lucene.BlogIndex;
import com.blog.service.background.Impl.*;

/**
 * title:InitComponent.java
 * description:初始化组件 把博主信息 根据博客类别分类信息 根据日期归档分类信息 
 * 		                   存放到application中，用以提供页面请求性能
 */
@Component
public class InitComponent implements ServletContextListener,ApplicationContextAware{
	@Autowired
	

	private static final Logger log=LoggerFactory.getLogger(InitComponent.class);
	
	//其实这个就是spring的IOC容器-spring上下文应用程序
	private static ApplicationContext applicationContext;
	
	@SuppressWarnings("static-access")
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext=applicationContext;
	}

	public void contextInitialized(ServletContextEvent servletContextEvent) {
		//application：整个javaWeb应用的页面作用域：生命周期是整个应用 ,可以往此对象中绑定一些经常需要访问的对象供某些经常访问的页面如首页使用
		ServletContext application=servletContextEvent.getServletContext();
		
		//Spring IOC容器中获取各个bean
		BloggerService bloggerService=(BloggerService) applicationContext.getBean("bloggerService");
		Blogger blogger=bloggerService.getBlogger(); // 查询博主信息
		blogger.setPassword(null);
		application.setAttribute("blogger", blogger);
		
		BlogTypeService blogTypeService=(BlogTypeService) applicationContext.getBean("blogTypeService");
		List<BlogType> blogTypeCountList=blogTypeService.countList(); // 查询博客类别以及博客的数量
		application.setAttribute("blogTypeCountList", blogTypeCountList);
		
		BlogService blogService=(BlogService) applicationContext.getBean("blogService");
		List<Blog> blogCountList=blogService.countList(); // 根据日期分组查询博客
		application.setAttribute("blogCountList", blogCountList);
		
		LinkService linkService=(LinkService) applicationContext.getBean("linkService");
		List<Link> linkList=linkService.getAllLink(); // 查询所有的友情链接信息
		application.setAttribute("linkList", linkList);
		
		String ctx=application.getRealPath("/");
		
		log.debug("根路径：{} ",ctx);
		application.setAttribute("ctx", ctx);
	}

	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}


}
