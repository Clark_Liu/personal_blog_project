package com.blog.service.background;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blog.dao.BloggerMapper;
import com.blog.entity.Blogger;
import com.blog.service.background.Impl.ILoginService;

@Service
public class LoginSerivce implements ILoginService{
	@Autowired
	private BloggerMapper bloggerMapper;
	
	public Blogger getBloggerByName(String name) {
		Blogger blogger = bloggerMapper.getBloggerByUserName(name);
		
		return blogger;
	}
}
