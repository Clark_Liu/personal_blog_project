package com.blog.service.background.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blog.dao.MenuMapper;
import com.blog.entity.Menu;
import com.blog.service.background.IMenuService;

@Service
public class MenuService implements IMenuService {
	@Autowired
	private MenuMapper menuMapper;
	
	
	public List<Menu> getParentMenu() {
		List<Menu> menuList = menuMapper.getParentMenuAll();
		return menuList;
	}

	public List<Menu> getChildMenu(String parentName) {
		List<Menu> menuList = null;
		if("常用操作".equals(parentName)) {
			menuList = menuMapper.getOftenMenu();
		}else {
			menuList = menuMapper.getChildMenuByParentName(parentName);
		}
		return menuList;
	}
}
