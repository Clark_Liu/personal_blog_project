package com.blog.service.background;

import java.util.HashMap;
import java.util.List;

import com.blog.entity.BlogType;

public interface IBlogTypeService {
	
	public List<BlogType> getBlogType();
	
	public BlogType getBlogTypeById(Integer id);
	
	public HashMap<String,Object> getAllBlogType(Integer page,Integer rows);
	
	public int addNewBlogType(BlogType blogType);
	
	public int updateBlogTypeById(BlogType blogType);

	public int removeBlogTyepByArrId(Long[] arrId);

	public List<BlogType> countList();

}
