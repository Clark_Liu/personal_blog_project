package com.blog.service.background;

import java.util.HashMap;
import java.util.List;

import com.blog.entity.Link;

public interface ILinkService {

	HashMap<String, Object> getAllLink(Integer page, Integer rows);

	Link getLinkById(int id);

	int removeLinkByIds(int[] arrId);
	
	int updateByPrimaryKeySelective(Link link);
	
	int addNewLink(Link link);
	List<Link> getAllLink();
}
