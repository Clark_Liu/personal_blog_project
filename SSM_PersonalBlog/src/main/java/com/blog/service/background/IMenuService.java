package com.blog.service.background;

import java.util.List;

import com.blog.entity.Menu;

public interface IMenuService {
	
	public List<Menu> getParentMenu();
	
	public List<Menu> getChildMenu(String parentName);
}
