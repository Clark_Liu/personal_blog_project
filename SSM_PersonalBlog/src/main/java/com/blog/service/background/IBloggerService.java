package com.blog.service.background;

import com.blog.entity.Blogger;

public interface IBloggerService {
	public Blogger getBloggerByUserName(String userName);

	public int updateBlogger(Blogger blogger);

	public Blogger getBlogger();

}
