package com.blog.service.background;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.blog.entity.Blog;

public interface IBlogService {

	
	
	public int saveBlog(Blog blog) ;
	
	
	
	public HashMap<String, Object> getAllBlog(Integer page, Integer rows);
	
	public List<Blog> getAllBlog();
	
	
	public Blog getBlogById(Integer id);
	
	public int editSaveBlog(Blog b);
	
	public int removeBlog(Long[] arrId);
	
	public void recoverBlog(Long[] arrId);
	
	public int getBlogAmountByBlogType(Integer blogTypeId);
	
	public HashMap<String,Object> searchBlogByTitle(Integer page,Integer rows,String title);



	public List<Blog> countList();





	public HashMap<String,Object> getBlogListBySearchCondition(HashMap<String, Object> map, Integer pageNo, Integer pagesize);



	public Blog getPrevBlog(Integer id);



	public Blog getNextBlog(Integer id);



}
