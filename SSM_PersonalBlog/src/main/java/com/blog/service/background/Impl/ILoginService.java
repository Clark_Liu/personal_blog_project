package com.blog.service.background.Impl;

import com.blog.entity.Blogger;

public interface ILoginService {
	
	
	public Blogger getBloggerByName(String name);
}
