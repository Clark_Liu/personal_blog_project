package com.blog.service.background.Impl;


import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blog.dao.CommentMapper;
import com.blog.entity.Comment;
import com.blog.service.background.ICommentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
@Service
public class CommentService implements ICommentService {
	@Autowired
	private CommentMapper commentMapper;
	
	
	
	public HashMap<String, Object> getReviewComments(Integer page, Integer rows) {
		PageHelper.startPage(page,rows);
		List<Comment> commentList = commentMapper.getToBeReviewComments();
		PageInfo<Comment> pageInfo = new PageInfo<Comment>(commentList);
		Long total = pageInfo.getTotal();
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("commentList", commentList);
		map.put("total", total);
		
		return map;
	}



	public int  updateReviewState(Integer temp, int[] arrId) {
		int i = commentMapper.updateReviewState(temp,arrId);
		return i;
	}



	public HashMap<String, Object> getAllComments(Integer page, Integer rows) {
		PageHelper.startPage(page,rows);
		List<Comment> commentList = commentMapper.getAllComments();
		PageInfo<Comment> pageInfo = new PageInfo<Comment>(commentList);
		Long total = pageInfo.getTotal();
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("commentList", commentList);
		map.put("total", total);
		return map;
		
	}



	public int removeComments(int[] arrId) {
		return commentMapper.removeCommentsByArrId(arrId);
	}



	public List<Comment> getCommentsByBlogId(Integer id) {
		List<Comment> commentList = commentMapper.getCommentsByBlogId(id);
		return commentList;
	}



	public int save(Comment comment) {
		int i = commentMapper.insertSelective(comment);
		return i;
	}
	
}
