package com.blog.service.background.Impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blog.dao.LinkMapper;
import com.blog.entity.Link;
import com.blog.service.background.ILinkService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service("linkService")
public class LinkService implements ILinkService {
	@Autowired
	LinkMapper linkMapper;
	
	
	public HashMap<String, Object> getAllLink(Integer page, Integer rows) {
		
		PageHelper.startPage(page,rows);
		List<Link> linkList = linkMapper.getAllLink();
		PageInfo<Link> pageInfo = new PageInfo<Link>(linkList);
		Long total = pageInfo.getTotal();
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("linkList", linkList);
		map.put("total", total);
		return map;
		
	}


	public Link getLinkById(int id) {
		
		Link link = linkMapper.selectByPrimaryKey(id);
		
		
		return link;
	}


	public int removeLinkByIds(int[] arrId) {
		return linkMapper.removeLinkByIds(arrId);
	}
	
	
	public int updateByPrimaryKeySelective(Link link) {
		return linkMapper.updateByPrimaryKeySelective(link);
	}
	
	
	public int addNewLink(Link link) {
		
		return linkMapper.insertSelective(link);
	}


	public List<Link> getAllLink() {
		return linkMapper.getAllLink();
	}
	
}
