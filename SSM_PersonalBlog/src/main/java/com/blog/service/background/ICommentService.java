package com.blog.service.background;

import java.util.HashMap;
import java.util.List;

import com.blog.entity.Comment;

public interface ICommentService {
	
	public HashMap<String, Object> getReviewComments(Integer page, Integer rows);
	
	
	public int  updateReviewState(Integer temp, int[] arrId);
	
	public HashMap<String, Object> getAllComments(Integer page, Integer rows);
	
	public int removeComments(int[] arrId);
	
	public List<Comment> getCommentsByBlogId(Integer id);


	public int save(Comment comment);
}
