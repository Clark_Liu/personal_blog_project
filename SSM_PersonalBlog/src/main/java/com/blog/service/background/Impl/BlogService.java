package com.blog.service.background.Impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blog.dao.BlogMapper;
import com.blog.entity.Blog;
import com.blog.service.background.IBlogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
@Service("blogService")
public class BlogService implements IBlogService {
	@Autowired
	private BlogMapper blogMapper;
	
	/**
	 * 保存博客
	 */
	public int saveBlog(Blog blog) {
		blog.setReleaseDate(new Date());
		blog.setCreateTime(new Date());
		int i = blogMapper.insertSelective(blog);
		return i;
	}
	
	/**
	 * 分页获取所有博客
	 */
	public HashMap<String, Object> getAllBlog(Integer page, Integer rows) {
		PageHelper.startPage(page,rows);
		List<Blog> blogList = blogMapper.getAllBlog();
		PageInfo<Blog> pageInfo = new PageInfo<Blog>(blogList);
		Long total = pageInfo.getTotal();
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("blogList", blogList);
		map.put("total", total);
		return map;
	}
	
	
	public List<Blog> getAllBlog() {
		List<Blog> blogList = blogMapper.getAllBlog();
		return blogList;
	}

	
	
	
	
	/**
	 * 根据博客id获取博客
	 */
	public Blog getBlogById(Integer id) {
		Blog b = blogMapper.selectByPrimaryKey(id);
		return b;
	}
	
	/**
	 * 修改保存博客
	 */
	public int editSaveBlog(Blog b) {
		b.setUpdateTime(new Date());
		int i = blogMapper.updateByPrimaryKeySelective(b);
		return i ;
	}
	/**
	 * 逻辑删除博客
	 */
	public int removeBlog(Long[] arrId) {
		int i =blogMapper.updateIsDeleteByArrId(arrId);
		return i;
	}
	/**
	 * 逻辑恢复博客
	 */
	public void recoverBlog(Long[] arrId) {
		blogMapper.recoverBlog(arrId);
	}
	/**
	 * 通过博客类型获取其所对应的博客数量
	 */
	public int getBlogAmountByBlogType(Integer blogTypeId) {
		int i = blogMapper.getBlogAmountByBlogTypeId(blogTypeId);
		return i;
	}
	/**
	 * 后台模糊搜索按照博客标题
	 */
	public HashMap<String, Object> searchBlogByTitle(Integer page, Integer rows, String title) {
		
		PageHelper.startPage(page,rows);
		List<Blog> blogList = blogMapper.selectBlogByTitle(title);
		PageInfo<Blog> pageInfo = new PageInfo<Blog>(blogList);
		Long total = pageInfo.getTotal();
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("blogList", blogList);
		map.put("total", total);
		return map;
	}

	public List<Blog> countList() {
		return blogMapper.countList();
	}

	

	public HashMap<String,Object> getBlogListBySearchCondition(HashMap<String, Object> map, Integer pageNo, Integer pagesize) {
		PageHelper.startPage(pageNo,pagesize);
		List<Blog> blogList = blogMapper.getBlogListBySearchCondition(map);
		PageInfo<Blog> pageInfo = new PageInfo<Blog>(blogList);
		Long total = pageInfo.getTotal();
		map.put("blogList", blogList);
		map.put("total", total);
		return map;
	}

	
	/**
	 * 获取上一篇博客
	 */
	public Blog getPrevBlog(Integer id) {
		Blog blog = blogMapper.getPrevBlog(id);
		
		return blog;
	}
	/**
	 * 获取下一篇博客
	 */
	public Blog getNextBlog(Integer id) {
		Blog blog = blogMapper.getNextBlog(id);
		return blog;
	}


}
