package com.blog.service.background.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blog.dao.BloggerMapper;
import com.blog.entity.Blogger;
import com.blog.service.background.IBloggerService;

@Service("bloggerService")
public class BloggerService implements IBloggerService {
	@Autowired
	private BloggerMapper bloggerMapper;
	
	
	
	public Blogger getBloggerByUserName(String userName) {
		return bloggerMapper.getBloggerByUserName(userName);
		
	}

	public int updateBlogger(Blogger blogger) {
		int i = bloggerMapper.updateByPrimaryKeySelective(blogger);
		return i;
	}

	/**
	 * 获取博主
	 * 目前版本博客为单独一个人的博客,不是平台
	 * 所以博主表中只有一个人
	 * @return
	 */
	public Blogger getBlogger() {
		Blogger blogger = bloggerMapper.getBlogger();
		return blogger;
	}
	
	

}
