package com.blog.service.background.Impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blog.dao.BlogTypeMapper;
import com.blog.entity.BlogType;
import com.blog.service.background.IBlogTypeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service("blogTypeService")
public class BlogTypeService implements IBlogTypeService {
	@Autowired
	private BlogTypeMapper blogTypeMapper;
	
	public List<BlogType> getBlogType() {
		List<BlogType> typeList = blogTypeMapper.getBlogType();
		return typeList;
	}

	public BlogType getBlogTypeById(Integer id) {
		BlogType blogType = blogTypeMapper.selectByPrimaryKey(id);
		return blogType;
	}

	public HashMap<String, Object> getAllBlogType(Integer page, Integer rows) {
		PageHelper.startPage(page,rows);
		List<BlogType> blogTypeList = blogTypeMapper.getBlogType();
		PageInfo<BlogType> pageInfo = new PageInfo<BlogType>(blogTypeList);
		Long total = pageInfo.getTotal();
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("blogTypeList", blogTypeList);
		map.put("total", total);
		return map;
	}

	public int addNewBlogType(BlogType blogType) {
		int i = blogTypeMapper.insertSelective(blogType);
		return i ;
	}

	public int updateBlogTypeById(BlogType blogType) {
		int i = blogTypeMapper.updateByPrimaryKeySelective(blogType);
		return i;
	}

	public int removeBlogTyepByArrId(Long[] arrId) {
		int i =blogTypeMapper.removeBlogTypeByArrId(arrId);
		return i;
	}

	public List<BlogType> countList() {
		List<BlogType> countList = blogTypeMapper.countList();
		
		
		return countList;
	}

}
