package com.blog.entity;

import java.util.Date;

public class Link {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column link.id
     *
     * @mbggenerated
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column link.link_name
     *
     * @mbggenerated
     */
    private String linkName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column link.link_url
     *
     * @mbggenerated
     */
    private String linkUrl;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column link.order_no
     *
     * @mbggenerated
     */
    private Integer orderNo;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column link.create_time
     *
     * @mbggenerated
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column link.update_time
     *
     * @mbggenerated
     */
    private Date updateTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column link.id
     *
     * @return the value of link.id
     *
     * @mbggenerated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column link.id
     *
     * @param id the value for link.id
     *
     * @mbggenerated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column link.link_name
     *
     * @return the value of link.link_name
     *
     * @mbggenerated
     */
    public String getLinkName() {
        return linkName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column link.link_name
     *
     * @param linkName the value for link.link_name
     *
     * @mbggenerated
     */
    public void setLinkName(String linkName) {
        this.linkName = linkName == null ? null : linkName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column link.link_url
     *
     * @return the value of link.link_url
     *
     * @mbggenerated
     */
    public String getLinkUrl() {
        return linkUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column link.link_url
     *
     * @param linkUrl the value for link.link_url
     *
     * @mbggenerated
     */
    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl == null ? null : linkUrl.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column link.order_no
     *
     * @return the value of link.order_no
     *
     * @mbggenerated
     */
    public Integer getOrderNo() {
        return orderNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column link.order_no
     *
     * @param orderNo the value for link.order_no
     *
     * @mbggenerated
     */
    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column link.create_time
     *
     * @return the value of link.create_time
     *
     * @mbggenerated
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column link.create_time
     *
     * @param createTime the value for link.create_time
     *
     * @mbggenerated
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column link.update_time
     *
     * @return the value of link.update_time
     *
     * @mbggenerated
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column link.update_time
     *
     * @param updateTime the value for link.update_time
     *
     * @mbggenerated
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}