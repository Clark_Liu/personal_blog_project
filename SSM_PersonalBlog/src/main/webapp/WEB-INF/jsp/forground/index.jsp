<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${pageTitle }</title>
</head>
<link rel="stylesheet" href="../../../static/bootstrap3/css/bootstrap.min.css">
<link rel="stylesheet" href="../../../static/bootstrap3/css/bootstrap-theme.min.css">
<link href="http://blog.java1234.com/favicon.ico" rel="SHORTCUT ICON">
<link rel="stylesheet" href="../../../static/css/blog.css">
<script src="../../../static/bootstrap3/js/jquery-1.11.2.min.js"></script>
<link href="../../../static/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
<script src="../../../static/bootstrap3/js/bootstrap.min.js"></script>
<style type="text/css">
	  body {
        padding-top: 10px;
        padding-bottom: 40px;
      }
</style>
<body>
<div class="container">
	<jsp:include page="./common/head.jsp"/>
	<jsp:include page="./common/menu.jsp"/>
	<div class="row">
		<div class="col-md-9">
			<jsp:include page="${mainPage }"></jsp:include> 
		</div>
		
		<div class="col-md-3">
						
			<div class="data_list">
				<div class="data_list_title">
					<a href="#" target="_blank"><img src="${ctx}/static/img/user_icon.png"/></a>
					博主信息
				</div>
				<div class="user_image">
					<a href="#" target="_blank">
						<img src="${ctx}/static/userFileUpload/headImg/${blogger.imageName }"/>
					</a>
				</div>
				<div class="nickName">${blogger.nickName }</div>
				<div class="userSign">(${blogger.sign })</div>
			</div>
			
			<div class="data_list">
				<div class="data_list_title">
					<img src="${ctx}/static/img/byType_icon.png"/>
					按博客类别
				</div>
				<div class="datas">
					<ul>
						<c:forEach var="blogTypeCount" items="${blogTypeCountList }">
							<li><span><a href="${ctx}/index.html?typeId=${blogTypeCount.id }" target="_blank">${blogTypeCount.typeName }(${blogTypeCount.blogCount })</a></span></li>
						</c:forEach>
					</ul>
				</div>
			</div>
			
			<div class="data_list">
				<div class="data_list_title">
					<img src="${ctx}/static/img/byDate_icon.png"/>
					按发布日期
				</div>
				<div class="datas">
					<ul>
						<c:forEach var="blogCount" items="${blogCountList }">
							<li><span><a href="${ctx}/index.html?releaseDateStr=${blogCount.releaseDateStr }" target="_blank">${blogCount.releaseDateStr }(${blogCount.blogCount })</a></span></li>
						</c:forEach>
					</ul>
				</div>
			</div>
			
			<div class="data_list">
				<div class="data_list_title">
					<img src="${ctx}/static/img/link_icon.png"/>
					友情链接
				</div>
				<div class="datas">
					<ul>
						<c:forEach var="link" items="${linkList }">
							<li><span><a href="${link.linkUrl }" target="_blank">${link.linkName }</a></span></li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<jsp:include page="./common/foot.jsp"/>
</div>
</body>
</html>