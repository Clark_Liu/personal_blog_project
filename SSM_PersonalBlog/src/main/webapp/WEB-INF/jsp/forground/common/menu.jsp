<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<script type="text/javascript">
	function checkData(){
		var q=document.getElementById("queryWord").value.trim();
		if(q==null || q==""){
			alert("请输入您要查询的关键字！");
			return false;
		}else if(q.indexOf("<") != -1){
			alert('请不要包含"<"字符');
			return false;
		}else{
			return true;
		}
	}
</script>
<div class="row">
	<div class="col-md-12" style="padding-top: 10px">
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/index"><font color="black"><strong>首页</strong></font></a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
		      <ul class="nav navbar-nav">
		        <!-- <li><a href="#"><font color="black"><strong>CSDN博客</strong></font color="black"></a></li>-->
		        <li><a href="/background/login" target="_blank"><font color="red"><strong>后台登录</strong></font></a></li> 
		        <li><a href="/getBloggerInfo" target="_blank"><font color="black"><strong>关于我</strong></font></a></li>
		        <!-- <li><a href="/getBloggerInfo" target="_blank"><font color="black"><strong>我们的团队</strong></font></a></li> -->
		        <li><a href="/download" target="_blank"><font color="black"><strong>本站源码下载</strong></font></a></li>
		      </ul>
		      <form action="/searchByWord" class="navbar-form navbar-right" role="search" method="post" onsubmit="return checkData()">
		        <div class="form-group" >
		          <input type="text" id="queryWord" name="queryWord" value="${q }" class="form-control" placeholder="请输入要查询的关键字...">
		        </div>
		        <button type="submit" class="btn btn-default">搜索</button>
		      </form>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
</div>