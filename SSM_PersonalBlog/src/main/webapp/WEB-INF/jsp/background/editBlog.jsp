<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
      td{
      	vertical-align:top;
      }
      tr{
      	height: 50px;
      }
</style>
</head>
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/default/easyui.css">   
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/icon.css">   
<script type="text/javascript" src="../../../static/easyui/jquery.min.js"></script>   
<script type="text/javascript" src="../../../static/easyui/jquery.easyui.min.js"></script>  
<script type="text/javascript" src="../../../static/easyui/locale/easyui-lang-zh_CN.js"></script> 
<!-- <script type="text/javascript" src="https://unpkg.com/wangeditor@3.1.1/release/wangEditor.min.js"></script>  -->
    <!-- 配置文件 -->
    <script type="text/javascript" src="../../../static/newUeditor/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="../../../static/newUeditor/ueditor.all.js"></script>


<body>
    
	<div id="p" class="easyui-panel" data-options="fit:true" style="padding:5px;" title="编写博客" iconCls="icon-write" >   
	 	<form id="myForm">
		 	<table  style="width:75%;margin-top:30px;margin-left:30px">
		 		<tr style="height:50px">
		 			<td style="width:80px">博客标题:</td>
		 			<td >
		 				<input type="text" id="blogTitle"  name="title" style="width:50%"/>
		 				<input type="hidden" id="id" name="id"/>
		 				<span style="color: red">*</span>
		 			</td>
		 		</tr>
		 		<tr>
		 			<td >所属类型:</td>
		 			<td>
		 				<select id="blogTypeSel" style="width:30%"></select>
		 				<span style="color: red">*</span>
		 				<input type="hidden" name="typeId" id="typeId">
		 			</td>
		 		</tr>
		 		<tr>
		 			<td >博客内容:</td>
		 			<td>
		 				<!-- <div id="editor" style="margin-bottom: 30px">
		      				<p>欢迎使用 <b>wangEditor</b> 富文本编辑器</p>
		  				</div> -->
		  				  <!-- 加载编辑器的容器 -->
					    <script id="container"  type="text/plain" style="width:100%;height:300px;">
    					    这里写你的初始化内容
 					   </script>
		  				
		  				
		  				<input type="hidden" name="summary" id="summary"/>
		  				<input type="hidden" name="content" id="content"/>
		 			</td>
		 		</tr>
		 		<tr>
		 			<td  valign="top">关键字:</td>
		 			<td>
		 				<input type="text" name="keyWord" id="keyWord" style="width:50%"/>&nbsp;(多个关键字用空格隔开)
		 			</td>
		 		</tr>
		 		<tr>
		 			<td></td>
		 			<td>
		 				<a id="btn" href="#" class="easyui-linkbutton" onclick="saveBlog()" data-options="iconCls:'icon-save'">发布博客</a>  
		 			</td>
		 		</tr>
		 	</table>
 		</form>
	</div>  
</body>
<script type="text/javascript">
	$(function(){
		$.ajax({
			async:false,
			url:"/blogType/getBlogType",
			type:"post",
			data:{},
			success:function(data){
				$("#blogTypeSel").append("<option value=''>请选择博客类别...</option>");
				data=$.parseJSON(data).typeList;
				for(var i in data){
					var opt = "<option id='"+data[i].id+"' value='"+data[i].id+"'>"+data[i].typeName+"</option>";
					$("#blogTypeSel").append(opt);
				}
			}
		})
		ue = UE.getEditor('container');
		$.ajax({
			async:false,
			url:"/blog/getBlogById",
			type:"post",
			data:{
				id:"${param.id}"
			},
			success:function(data){
				data = $.parseJSON(data).b;
				$("#id").val(data.id);
				$("#blogTitle").val(data.title);
				var opts = $("#blogTypeSel option");
				for(var i in opts){
					if($(opts[i]).val() == data.typeId){
						$(opts[i]).prop("selected",true);
						break;
					}
				}
				ue.ready(function() {
					ue.setContent("");
					ue.setContent(data.content);
				});
				
				$("#keyWord").val(data.keyWord);
				
			}
		})
	});
 	/* var E = window.wangEditor;
	var editor = new E('#editor');
	editor.customConfig.zIndex = 1;
	editor.create(); */
	function saveBlog(){
		$("#content").val(ue.getContent());
		$("#summary").val(ue.getContentTxt().substr(0,155));
		$("#blogTypeSel option").each(function(){
			if($(this).prop("selected")){
				$("#typeId").val($(this).val());
			}
		})
		$('#myForm').form({    
		    url:"/blog/editSaveBlog",    
		    onSubmit: function(){
		    	if($('#blogTitle').val()==""){
		    		$.messager.alert('警告','请填写博客标题!','warning'); 
			    	$('.panel-tool-close').hide(); 
		    		return false;
		    	}
		    	if($("#typeId").val()==""){
		    		$.messager.alert('警告','请选择博客类别!','warning'); 
			    	$('.panel-tool-close').hide(); 
		    		return false;
		    	}
		    },    
		    success:function(data){ 
		    	var topTab = top.jQuery; 
		    	if(data=="success"){
		    		$.messager.alert('消息',"保存成功!",'info',function(){
		    			UE.getEditor('container').setContent("");
		    			$('#myForm').form("clear");
				    	parent.reloadTabGrid("博客信息管理");
				    	topTab("#myTabs").tabs('close',"修改博客");
		    		});
			    	$('.panel-tool-close').hide(); 
		    	}else{
		    		$.messager.alert('消息',"保存失败!",'warning')
		    	}
		    }    
		});    
		$('#myForm').submit();  
	}
</script>
</html>