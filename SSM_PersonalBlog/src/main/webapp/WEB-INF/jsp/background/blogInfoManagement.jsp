<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/default/easyui.css">   
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/icon.css">   
<script type="text/javascript" src="../../../static/easyui/jquery.min.js"></script>   
<script type="text/javascript" src="../../../static/easyui/jquery.easyui.min.js"></script>  
<script type="text/javascript" src="../../../static/easyui/locale/easyui-lang-zh_CN.js"></script> 
<body>
<table id="blogTable" fit="true"></table>
<div id="tb">
	<a class="easyui-linkbutton" iconCls="icon-edit" onclick="editBlog()" plain="true">修改</a>
	<a class="easyui-linkbutton" iconCls="icon-remove" onclick="removeBlog()" plain="true">删除</a><br>
	标题：&nbsp;<input type="text" name="title" id="title"/>
	<a class="easyui-linkbutton" iconCls="icon-search" onclick="searchBlog()" plain="true">搜索</a>
	(温馨提示:清空搜索框可查询所有数据)
</div>
</body>
<script type="text/javascript">
	$(function(){
		$("#blogTable").datagrid({
			url:"/blog/getAllBlog",
			title:"博客列表",
			toolbar:"#tb",
			fitColumns:true,
			rownumbers:true,
			pagination:true,
			//singleSelect:true,
			columns:[[
				{field:'ck',checkbox:"true"},
				{field:"id",title:"编号",width:3,align:"center"},
				{field:"title",title:"标题",width:10,align:"center"},
				{field:"releaseDate",title:"发布日期",width:2,align:"center"},
				{field:"typeName",title:"博客类型",width:5,align:"center"}
			]]
		})
	})
	
	
	 window.top["reload_blogTable"]=function(){
		$("#blogTable").datagrid( "load");
	};
	
	
	function editBlog(){
		data = $("#blogTable").datagrid("getSelections");
		if(data.length==0){
			$.messager.alert('警告','请选择要修改的博客!','warning'); 
			return;
		}else if(data.length>1){
			$.messager.alert('警告','请选择一条要修改的博客','warning'); 
			return;
		}else{
			var row=data[0];
			//获取到父级对象
			var jq = top.jQuery;    
            if (jq("#myTabs").tabs('exists', "修改博客")){    
                jq("#myTabs").tabs('select', "修改博客");    
            } else {  
                var content = '<iframe scrolling="auto" frameborder="0"  src="/background/editBlog?id='+data[0].id+'" style="width:100%;height:100%;"></iframe>';     
                jq("#myTabs").tabs('add',{    
                                    title:"修改博客",    
                                    content:content,    
                                    closable:true    
                                  });    
             }  
		}
	}
	
	function removeBlog(){
		data = $("#blogTable").datagrid("getSelections");
		if(data.length==0){
			$.messager.alert('警告','请选择要要删除的博客!','warning'); 
		}else{
			$.messager.confirm("温馨提示","是否删除?",function(r){
				if(r){
					var arrId = [];
					for(var i in data){
						arrId.push(data[i].id);
					}
					console.log(arrId);
					$.ajax({
						async:false,
						traditional:true,
						type:"post",
						url:"/blog/removeBlog",
						data:{arrId:arrId},
						success:function(data){
							if(data=="success"){
								$.messager.alert('消息','删除成功!','info',function(){
									$("#blogTable").datagrid("reload");
								}); 
								$('.panel-tool-close').hide(); 
							}else{
								$.messager.alert("警告","删除失败!",'warning');
							}
						}
					})	
				}
			})
		}
	}
	
	
	function searchBlog(){
		var title= $("#title").val();
		
		$("#blogTable").datagrid({
			url:"/blog/searchBlogByTitle?title="+title,
			title:"博客列表",
			toolbar:"#tb",
			fitColumns:true,
			rownumbers:true,
			pagination:true,
			//singleSelect:true,
			columns:[[
				{field:'ck',checkbox:"true"},
				{field:"id",title:"编号",width:3,align:"center"},
				{field:"title",title:"标题",width:10,align:"center"},
				{field:"releaseDate",title:"发布日期",width:2,align:"center"},
				{field:"typeName",title:"博客类型",width:5,align:"center"}
			]]
		})
		
		
		
		
	}
</script>
</html>