<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/default/easyui.css">   
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/icon.css">   
<script type="text/javascript" src="../../../static/easyui/jquery.min.js"></script>   
<script type="text/javascript" src="../../../static/easyui/jquery.easyui.min.js"></script>  
<script type="text/javascript" src="../../../static/easyui/locale/easyui-lang-zh_CN.js"></script> 
<body>
<table id="commentTable"></table>
<div id="tb">
	<a class="easyui-linkbutton" iconCls="icon-ok" onclick="reviewIsPass(this)" flg="true" plain="true">审核通过</a>
	<a class="easyui-linkbutton" iconCls="icon-cancel" onclick="reviewIsPass(this)" flg="false" plain="true">审核不通过</a>
</div>
</body>
<script type="text/javascript">
	$(function(){
		$("#commentTable").datagrid({
			url:"/comment/reviewComments",
			title:"待审核评论列表",
			fit:true,
			toolbar:"#tb",
			fitColumns:true,
			rownumbers:true,
			pagination:true,
			//singleSelect:true,
			columns:[[
				{field:'ck',checkbox:"true"},
				{field:"id",title:"编号",width:2,align:"center"},
				{field:"blogTitle",title:"博客标题",width:5,align:"center"},
				{field:"userIp",title:"用户IP",width:2,align:"center"},
				{field:"content",title:"评论内容",width:10,align:"center"},
				{field:"commentDate",title:"评论日期",width:2,align:"center"}
			]]
		})
	})
	function reviewIsPass(e){
		data = $("#commentTable").datagrid("getSelections");
		if(data.length>0){
			$.messager.confirm('消息', '确定要执行此操作吗?', function(r){
				if (r){
					var arrId=[];
					for(var i in data){
						arrId.push(data[i].id);
					}
					var flg = $(e).attr("flg");
					var temp;
					if(flg == "true"){
						temp = 1;
					}else{
						temp = -1;
					}
					$.ajax({
						async:false,
						traditional:true,
						type:"post",
						url:"/comment/updateReviewState",
						data:{
							temp:temp,
							arrId:arrId
						},
						success:function(data){
							if(data=="success"){
								$.messager.alert("消息","操作成功","info",function(){
									$("#commentTable").datagrid("reload");
								});
							}else{
								$.messager.alert("消息","操作失败","warning");
							}
						}
					})
				}
			});
		}else{
			$.messager.alert("消息","请选择一条评论","warning");
		}
	}

</script>
</html>