<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<style type="text/css">
      
      td{
      	vertical-align:top;
      }
      tr{
      	height: 50px;
      }
      
</style>
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/default/easyui.css">   
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/icon.css">   
<script type="text/javascript" src="../../../static/easyui/jquery.min.js"></script>   
<script type="text/javascript" src="../../../static/easyui/jquery.easyui.min.js"></script>  
<script type="text/javascript" src="../../../static/easyui/locale/easyui-lang-zh_CN.js"></script> 
<!-- <script type="text/javascript" src="https://unpkg.com/wangeditor@3.1.1/release/wangEditor.min.js"></script>  -->
    <!-- 配置文件 -->
    <script type="text/javascript" src="../../../static/newUeditor/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="../../../static/newUeditor/ueditor.all.js"></script>

<body>
<div id="p" class="easyui-panel" data-options="fit:true" style="padding:5px;" title="修改个人信息" iconCls="icon-write" >
<form id="myForm"  enctype="multipart/form-data" method="post">
	<table style="width:75%;margin-top:30px;margin-left:30px">
		<tr>
			<td></td>
			<td style="color: red">(温馨提示:修改信息后会自动刷新系统,请提前保存后其他页面的内容.)</td>
		</tr>
		<tr>
			<td style="width:10%">用户名:</td>
			<td>
				<input style="width:30%" type="text" id="userName" name="userName"/><span style="color: red">*</span>
			</td>
		</tr>
		<tr>
			<td>昵称:</td>
			<td>
				<input style="width:30%" type="text" id="nickName" name="nickName"/>
				<input type="hidden" name="id" id="id"/>
			</td>
		</tr>
		<tr>
			<td>个性签名:</td>
			<td>
				<input style="width:50%" type="text" id="sign" name="sign"/>
			</td>
		</tr>
		<tr>
			<td>个性头像:</td>
			<td>
				<input type="file" id="imageName" name="file" />
			</td>
		</tr>
		<tr>
			<td>个人简介:</td>
			<td>
				<script id="container" name='profile'  type="text/plain" style="width:100%;height:300px;">
 				</script>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<a id="btn" href="#" class="easyui-linkbutton" onclick="savePersonalInfo()" data-options="iconCls:'icon-save'">提交</a>  
			</td>
		</tr>
	</table>
	</form>
</div>
</body>
<script type="text/javascript">
$(function(){
	
	 var ue = UE.getEditor('container');
	
	 $.ajax({
		 url:"/blogger/getBlogger",
		 async:false,
		 type:"post",
		 data:{},
		 success:function(data){
		 	data = $.parseJSON(data).user;
		 	$("#id").val(data.id);
			$("#userName").val(data.userName);
			$("#nickName").val(data.nickName);
			$("#sign").val(data.sign);
			ue.ready(function() {
				ue.setContent("");
				ue.setContent(data.profile);
			});
		 }
	 })
	 
	 
})

function savePersonalInfo(){
	
	
	$('#myForm').form('submit', {    
	    url:"/blogger/saveBloggerInfo",    
	    onSubmit: function(){    
	        if($("#userName").val() ==""){
				$.messager.alert("警告","用户名不可为空!","warning");
				return false;
			}
	    },    
	    success:function(data){   
	    	if(data == "success"){
	    		$.messager.alert("消息","操作成功","info",function(){
	    			window.parent.location.replace("/background/backgroundIndex");
	    		});	    		
	    	}
	    }    
	}); 
	
	
	
	
	
	
}



</script>
</html>