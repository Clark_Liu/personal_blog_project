<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/default/easyui.css">   
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/icon.css">   
<script type="text/javascript" src="../../../static/easyui/jquery.min.js"></script>   
<script type="text/javascript" src="../../../static/easyui/jquery.easyui.min.js"></script>  
<script type="text/javascript" src="../../../static/easyui/locale/easyui-lang-zh_CN.js"></script>
<body>

<div id="linkDialog">
		<form id="link_form" method="post">
			<table align="center" cellspacing="5px" style="margin-top: 15px;">
				<tr >
					<td >友情链接名称: </td>
					<td>
						<input type="text" id="linkName" name="linkName">
						<input type="hidden" id="id" name="id"/>
					</td>
				</tr>
				<tr>
					<td>友情链接地址:</td>
					<td>
						<input type="text" id="linkUrl" name="linkUrl"/>
					</td>
				</tr>
				<tr>
					<td >排列序号:</td>
					<td><input type="text" id="orderNo" name="orderNo"/></td>
				</tr>
			</table>
		</form>
	</div>
<div id="linkTable"></div>
<div id="tb">
	<a class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addLink()">添加</a>
	<a class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editLink()">修改</a>
	<a class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeLink()">删除</a>
</div>
<div id="tt">
		<a class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="save()">保存</a>
		<a class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="cancel()">取消</a>
</div>
</body>
<script type="text/javascript">
	$(function(){
		$("#linkTable").datagrid({
			url:"/link/getAllLink",
			title:"友情链接管理",
			fit:true,
			toolbar:"#tb",
			fitColumns:true,
			rownumbers:true,
			pagination:true,
			//singleSelect:true,
			columns:[[
				{field:'ck',checkbox:"true"},
				{field:"id",title:"编号",width:2,align:"center"},
				{field:"linkName",title:"友情链接名称",width:5,align:"center"},
				{field:"linkUrl",title:"友情链接地址",width:10,align:"center"},
				{field:"orderNo",title:"排序编号",width:2,align:"center"}
			]]
		})
		$("#linkDialog").dialog({
			width:300,
			height:200,
			buttons:"#tt",
			closed:true
		})
	})
	function addLink(){
		$("#link_form").form("clear");
		$("#linkDialog").dialog("open");
		$("#linkDialog").dialog("setTitle","添加");
	}
	function editLink(){
		var data = $("#linkTable").datagrid("getSelections");
		if(data.length==0){
			$.messager.alert("温馨提示","请选择要修改的友情链接!","info");
		}else if(data.length>1){
			$.messager.alert("温馨提示","请选择一条要修改的友情链接!","info");
		}else{
			$("#link_form").form("clear");
			$("#linkDialog").dialog("open");
			$("#linkDialog").dialog("setTitle","修改");
			$.ajax({
				async:false,
				type:"post",
				url:"/link/getLinkById",
				data:{
					id:data[0].id
				},
				success:function(data){
					data=$.parseJSON(data).link;
					console.log(data);
					$("#id").val(data.id);
					$("#linkName").val(data.linkName);
					$("#linkUrl").val(data.linkUrl);
					$("#orderNo").val(data.orderNo);
				}
			})
		}
	}
	function removeLink(){
		var data = $("#linkTable").datagrid("getSelections");
		if(data.length>0){
			$.messager.confirm("温馨提示","是否删除?",function(r){
				if(r){
					var arrId = [];
					for(var i in data){
						arrId.push(data[i].id);
					}
					console.log(arrId)
					$.ajax({
						url:"/link/removeLinkByIds",
						type:"post",
						async:true,
						traditional:true,
						data:{
							arrId:arrId
						},
						success:function(data){
							if(data=='success'){
								$.messager.alert("温馨提示","删除成功!","info",function(){
									$("#linkTable").datagrid("load");
								});
							}else{
								$.messager.alert("温馨提示","删除失败!","info");
							}
						}
					})
				}
			})
		}else{
			$.messager.alert("温馨提示","请选中要删除的数据","info");
		}
	}
	function save(){
		var url;
		var id = $("#id").val();
		if(id){
			url='/link/updateLinkById';
		}else{
			url='/link/addNewLink';
		}
		$("#link_form").form("submit",{
			url:url,
			success:function(data){
				var i ;
				if(data=='success'){
					i = $.messager.alert("温馨提示","操作成功!","info",function(){
						$("#linkDialog").dialog("close");
						$("#linkTable").datagrid("load");
					});
					//2秒后自动关闭确认框
					setTimeout(function(){
						i.window("close");
						$("#linkDialog").dialog("close");
						$("#linkTable").datagrid("load");
					}, 2000);
				}else{
					i = $.messager.alert("温馨提示","操作失败!","info");
					setTimeout(function(){
						i.window("close");
					}, 2000);
				}
			}
		});
	}
	function cancel(){
		$("#linkDialog").dialog("close");
	}
	
	
	
	
	
	












</script>




</html>