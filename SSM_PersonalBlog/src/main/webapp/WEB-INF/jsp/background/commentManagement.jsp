<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/default/easyui.css">   
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/icon.css">   
<script type="text/javascript" src="../../../static/easyui/jquery.min.js"></script>   
<script type="text/javascript" src="../../../static/easyui/jquery.easyui.min.js"></script>  
<script type="text/javascript" src="../../../static/easyui/locale/easyui-lang-zh_CN.js"></script> 
<body>
<div id="commentTable"></div>
<div id="tb">
	<a class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeComment()">删除</a>
</div>
</body>
<script type="text/javascript">
	$(function(){
		
		
		
		window.top["reload_commentTable"]=function(){
			$("#commentTable").datagrid( "load");
		};
		
		$("#commentTable").datagrid({
			url:"/comment/getAllComments",
			title:"评论列表",
			fit:true,
			toolbar:"#tb",
			fitColumns:true,
			rownumbers:true,
			pagination:true,
			//singleSelect:true,
			columns:[[
				{field:'ck',checkbox:"true"},
				{field:"id",title:"编号",width:2,align:"center"},
				{field:"blogTitle",title:"博客标题",width:5,align:"center"},
				{field:"userIp",title:"用户IP",width:2,align:"center"},
				{field:"content",title:"评论内容",width:10,align:"center"},
				{field:"commentDate",title:"评论日期",width:2,align:"center"},
				{field:"state",title:"评论状态",width:2,align:"center"}
			]]
		})
	})
	
	function removeComment(){
		data = $("#commentTable").datagrid("getSelections");
		if(data.length>0){
			$.messager.confirm('消息', '确定要删除选择的评论吗?', function(r){
				if(r){
					var arrId=[];
					for(var i in data){
						arrId.push(data[i].id);
					}
					$.ajax({
						async:false,
						traditional:true,
						type:"post",
						url:"/comment/removeComments",
						data:{
							arrId:arrId
						},
						success:function(data){
							if(data=="success"){
								$.messager.alert("消息","操作成功","info");
								$("#commentTable").datagrid("load");
							}else{
								$.messager.alert("消息","操作失败","warning");
							}
						}
					})
				}
			})
		}else{
			$.messager.alert("消息","请选择一条要删除的评论","warning");
		}
	}
	

</script>
</html>