<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/default/easyui.css">   
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/icon.css">   
<script type="text/javascript" src="../../../static/easyui/jquery.min.js"></script>   
<script type="text/javascript" src="../../../static/easyui/jquery.easyui.min.js"></script>  
<script type="text/javascript" src="../../../static/easyui/locale/easyui-lang-zh_CN.js"></script> 
<body>
<!-- 定义新增对话框 -->
	<div id="blogTypeDialog">
		<form id="blogType_form" method="post">
			<table align="center" cellspacing="5px" style="margin-top: 15px;">
				<tr >
					<td >博客类别名称: </td>
					<td>
						<input type="text" id="typeName" name="typeName">
						<input type="hidden" id="id" name="id"/>
					</td>
				</tr>
				<tr>
					<td >排列序号:</td>
					<td><input type="text" id="orderNo" name="orderNo"/></td>
				</tr>
			</table>
		</form>
	</div>
<table id="blogTypeTable"></table>
<div id="tb">
	<a class="easyui-linkbutton" plain="true" iconCls="icon-add" onclick="addBlogType()">添加</a>
	<a class="easyui-linkbutton" plain="true" iconCls="icon-edit" onclick="editBlogType()">修改</a>
	<a class="easyui-linkbutton" plain="true" iconCls="icon-remove" onclick="removeBlogType()">删除</a>
</div>
<div id="tt">
		<a class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="save()">保存</a>
		<a class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="cancel()">取消</a>
	</div>
</body>
<script type="text/javascript">
	$(function(){
		$("#blogTypeTable").datagrid({
			url:"/blogType/getAllBlogType",
			title:"博客类别列表",
			fit:true,
			toolbar:"#tb",
			fitColumns:true,
			rownumbers:true,
			pagination:true,
			//singleSelect:true,
			columns:[[
				{field:'ck',checkbox:"true"},
				{field:"id",title:"编号",width:2,align:"center"},
				{field:"typeName",title:"博客类别标题",width:10,align:"center"},
				{field:"orderNo",title:"排列序号",width:2,align:"center"},
				{field:"blogAmount",title:"博客数量",width:2,align:"center"}
			]]
		})
		
		
		$("#blogTypeDialog").dialog({
			width:300,
			height:200,
			buttons:"#tt",
			closed:true
		})
	})
	function addBlogType(){
		$("#blogType_form").form("clear");
		$("#blogTypeDialog").dialog("open");
		$("#blogTypeDialog").dialog("setTitle","添加");
	}
	function editBlogType(){
		var data = $("#blogTypeTable").datagrid("getSelections");
		if(data.length==0){
			$.messager.alert("温馨提示","请选择要修改的类别!","info");
		}else if(data.length>1){
			$.messager.alert("温馨提示","请选择一条要修改的类别!","info");
		}else{
			$("#blogType_form").form("clear");
			$("#blogTypeDialog").dialog("open");
			$("#blogTypeDialog").dialog("setTitle","修改");
			$.ajax({
				async:false,
				type:"post",
				url:"/blogType/getBlogTypeById",
				data:{
					id:data[0].id
				},
				success:function(data){
					data=$.parseJSON(data).blogType;
					console.log(data);
					$("#id").val(data.id);
					$("#typeName").val(data.typeName);
					$("#orderNo").val(data.orderNo);
				}
			})
		}
	}
	function removeBlogType(){
		var data = $("#blogTypeTable").datagrid("getSelections");
		for(var i in data){
			if(data[i].blogAmount>0){
				$.messager.alert("温馨提示","请确保所选类别下没有所属的博客!","info");
				return;
			}
		}
		if(data.length>0){
			$.messager.confirm("温馨提示","是否删除?",function(r){
				if(r){
					var arrId = [];
					for(var i in data){
						arrId.push(data[i].id);
					}
					console.log(arrId)
					$.ajax({
						url:"/blogType/removeBlogTypeById",
						type:"post",
						async:true,
						traditional:true,
						data:{
							arrId:arrId
						},
						success:function(data){
							if(data=='success'){
								$.messager.alert("温馨提示","删除成功!","info",function(){
									$("#blogTypeTable").datagrid("load");
								});
							}else{
								$.messager.alert("温馨提示","删除失败!","info");
							}
						}
					})
				}
			})
		}else{
			$.messager.alert("温馨提示","请选中要删除的数据","info");
		}
	}
	function save(){
		var url;
		var id = $("#id").val();
		if(id){
			url='/blogType/updateBlogTypeById';
		}else{
			url='/blogType/addNewBlogType';
		}
		$("#blogType_form").form("submit",{
			url:url,
			success:function(data){
				var i ;
				if(data=='success'){
					i = $.messager.alert("温馨提示","操作成功!","info",function(){
						$("#blogTypeDialog").dialog("close");
						$("#blogTypeTable").datagrid("load");
					});
					//2秒后自动关闭确认框
					setTimeout(function(){
						i.window("close");
						$("#blogTypeDialog").dialog("close");
						$("#blogTypeTable").datagrid("load");
					}, 2000);
				}else{
					i = $.messager.alert("温馨提示","操作失败!","info");
					setTimeout(function(){
						i.window("close");
					}, 2000);
				}
			}
		});
	}
	function cancel(){
		$("#blogTypeDialog").dialog("close");
	}
</script>
</html>