<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>博客后台管理系统</title>
</head>
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/default/easyui.css">   
<link rel="stylesheet" type="text/css" href="../../../static/easyui/themes/icon.css">   
<script type="text/javascript" src="../../../static/easyui/jquery.min.js"></script>   
<script type="text/javascript" src="../../../static/easyui/jquery.easyui.min.js"></script>  
<script type="text/javascript" src="../../../static/easyui/locale/easyui-lang-zh_CN.js"></script>  
<body>

 <div id="cc" class="easyui-layout" fit="true" style="width:500px;height:500px">
	<div data-options="region:'north'" style="height:100px;background: url('../../../static/img/banner-pic.gif') no-repeat;background-size: cover;">
		<table  style="padding: 5px" width="100%">
			<tr>
				<td width="50%">
					<h1>个人博客管理系统</h1>
				</td>			
				<td  valign="bottom" align="right" width="50%">
					您好,${userName }
				</td>
			</tr>
		</table>
	</div>
	<div data-options="region:'west',title:'导航菜单'"  style="width:200px">
		<div id="myAccordion" class="easyui-accordion" fit="true">
		</div>
	</div>
	<div data-options="region:'south'"  style="height:30px;background: url('../../../static/img/banner-pic.gif') no-repeat;background-size: cover;">
		<center style="margin-top: 5px">版权信息</center>
	</div>
	<div data-options="region:'center'" style="padding:5px;background:#eee;">
		<div id="myTabs" class="easyui-tabs" fit="true">
			<div title="欢迎页">
				<h1>欢迎登陆个人博客管理系统</h1>
			</div>
		</div>
	</div>
</div> 

<!-- 修改密码对话框 -->
	<div id="updatePassword">
		<form id="updatePassword_form" method="post">
			<table align="center" cellspacing="5px" style="margin-top: 15px;">
				<tr >
					<td >用户名: </td>
					<td>
						<input type="text" id="userName" name="userName">
						<input type="hidden" id="id" name="id"/>
					</td>
				</tr>
				<tr>
					<td>新密码:</td>
					<td>
						<input type="text" id="password" name="password"/>
					</td>
				</tr>
				<tr>
					<td >确认新密码:</td>
					<td><input type="text" id="newPassword" name="newPassword"/></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="tt">
		<a class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="save()">保存</a>
		<a class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="cancel()">取消</a>
	</div>


</body>
<script type="text/javascript">
	$(function(){
		//选定某个选项卡后刷新选项卡中的table数据,保持实时更新数据
		$("#myTabs" ).tabs({
			onSelect:function(title,index){
				if(title=="评论信息管理"){
		             window.top.reload_commentTable.call();
	            }
			}
		})
		$.ajax({
			type:"post",
			async:false,
			data:{},
			url:"/menu/getParentMenu",
			success:function(data){
				data = $.parseJSON(data).menuList;
				for(var i in data){
					if(i==0){
						$("#myAccordion").accordion("add",{
							title:data[i].menuName,
							iconCls:data[i].menuIcon,
							selected:true,
							animate:true,
							content:"<ul id='"+data[i].menuName+"Tree' ></ul>"
						});
						$("#"+data[i].menuName+"Tree").tree({
							url:"/menu/getChildMenu",
							queryParams:{
								parentName:data[i].menuName
		               		},
							animate:true,
							lines : true,
							onClick:function(node){
								var myTab = $("#myTabs");
								if(myTab.tabs("exists",node.text)){
									myTab.tabs("select",node.text)
								}else{
									console.log(node);
									myTab.tabs("add",{
										title:node.text,
										closable:true,
										content:"<iframe src='"+node.url+"' style='width:100%;height:100%' frameborder=0></iframe>"
									})
								}
							}
						})
					}else{
						$("#myAccordion").accordion("add",{
							title:data[i].menuName,
							iconCls:data[i].menuIcon,
							animate:true,
							selected:false,
							content:"<ul id='"+data[i].menuName+"Tree' ></ul>"
						})
					}
				}
			}
		});
		$("#myAccordion").accordion({
			onSelect:function(title,index){
				var tmp = $("#myAccordion").accordion("getPanel",index).children();
				if($(tmp[0]).children().length==0){
					$("#"+title+"Tree").tree({
						url:"/menu/getChildMenu",
						queryParams:{
							parentName:title
	               		},
						animate:true,
						lines : true,
						onClick:function(node){
							var myTab = $("#myTabs");
							if(myTab.tabs("exists",node.text)){
								myTab.tabs("select",node.text)
							}else if(node.text == "安全退出"){
								$.messager.confirm('确认对话框', '您想要退出该系统吗？', function(r){
									if (r){
										window.location.replace(node.url);
									}
								});
							}else if(node.text == "修改密码"){
								$("#updatePassword_form").form("clear");
								$("#updatePassword").dialog("open");
								$("#updatePassword").dialog("setTitle","修改密码");
								$("#userName").val("${userName }");
								$("#id").val("${userId }");
							}else if(node.text == '刷新缓存+清楚临时数据'){
								var result = $.ajax({url:"/refreshSystem",async:false});
								if(result.responseText == "success"){
									$.messager.alert("消息","刷新成功!","info");
									
								}
							}else{
								myTab.tabs("add",{
									title:node.text,
									closable:true,
									content:"<iframe src='"+node.url+"' style='width:100%;height:100%' frameborder=0></iframe>"
								})
							}
						}
					})
				};
			}
		})
		
		$("#updatePassword").dialog({
			width:300,
			height:200,
			buttons:"#tt",
			closed:true
		})
	})
	function selectTabRefurbish(){
		$("#myTabs" ).tabs({
			onSelect:function(title,index){
				if(title=="评论信息管理"){
		             window.top.reload_commentTable.call();
	            }
			}
		})
	}
	function reloadTabGrid(title){
      	if ($("#myTabs" ).tabs('exists', title)) {
            $( '#myTabs').tabs('select' , title);
            if(title=="博客信息管理"){
            	 window.top.reload_blogTable.call();
            }
            
      	}
    }
	
	function save(){
		$("#updatePassword_form").form("submit",{
			url:"/blogger/updatePassword",
			onSubmit:function(){
				if($("#userName").val()==""){
					$.messager.alert("警告","用户名不可为空","warning");
					return false;
				}
				if($("#password").val()==""){
					$.messager.alert("警告","密码不可为空","warning");
					return false;
				}
				if($("#newPassword").val()==""){
					$.messager.alert("警告","确认密码不可为空","warning");
					return false;
				}
				if($("#password").val() != $("#newPassword").val()){
					$.messager.alert("警告","两次密码输入不一样","warning");
					return false;
				}	
			},
			success:function(data){
				if(data == "success"){
		    		$.messager.alert("消息","操作成功,并退出账号重新登录","info",function(){
		    			window.location.replace("/doLogout");
		    		});	    		
		    	}else{
		    		$.messager.alert("警告","操作失败","warning");
		    	}
			}
		});
	}
	function cancel(){
		$("#updatePassword").dialog("close");
	}
	
	
	
</script>
</html>